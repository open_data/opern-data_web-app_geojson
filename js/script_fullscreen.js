/*
 Overlay der Personas
 */
$(document).ready(function() {
    // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();
});

/*
 Adolfinum Icon
 */
var adolfinum_icon = L.icon({
    iconUrl : 'data/icon/adolfinum_logo.png',
    iconSize : [ 30, 20 ] // size of the icon
});

/*
 Adolfinum Gymnasium als Ausgangspunkt erstellen
 */
var adolfinum = L.marker([ 51.453369739245765, 6.62991113902304 ], {
    icon : adolfinum_icon
}).bindPopup('Adolfinum Gymnasium Moers');
var adolfinum_layer = L.layerGroup([ adolfinum ]);

/*
 Default Einstellungen Karte (Rathaus Moers + Zoom Stufe)
 */
var map_full = L.map('map_full').setView([ 51.453388, 6.629645 ], 13);

/*
 OpenStreetMap einbinden http://{s}.tile.osm.org/{z}/{x}/{y}.png
 */
var openstreetmap = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution : '&copy; <a href="http://osm.org/copyright" target="_blank">OpenStreetmap</a> contributors; &copy; 2014 <a href="https://github.com/coryasilva/Leaflet.ExtraMarkers" target="_blank">coryasilva</a>'
}).addTo(map_full);

/*
 Icons
 */
var bike_icon = L.icon({
    iconUrl : 'data/icon/bike.png',
    iconSize : [ 35, 25 ] // size of the icon
});
var school_icon = L.icon({
    iconUrl : 'data/icon/school.png',
    iconSize : [ 40, 60 ] // size of the icon
});
var kitas_icon = L.ExtraMarkers.icon({
    markerColor : 'purple',
    shape : 'circle',
});

var telepano_360_grad_icon = L.ExtraMarkers.icon({
    markerColor : 'red',
    shape : 'circle',
});
var badestaetten_icon = L.ExtraMarkers.icon({
    markerColor : 'orange',
    shape : 'circle',
});
var hundekot_icon = L.ExtraMarkers.icon({
    markerColor : 'blue',
    shape : 'circle',
});
var sitzplaetze_icon = L.ExtraMarkers.icon({
    markerColor : 'cyan',
    shape : 'circle',
});
var stolpersteine_icon = L.ExtraMarkers.icon({
    markerColor : 'green-light',
    shape : 'circle',
});

/*
 var kitaIcon = L.icon({
 iconUrl: 'data/icon/kita.jpg',
 iconSize: [45, 35] // size of the icon
 });
 */

/*
 GeoJson Data
 */
var gruenflaechen_mo = L.geoJSON(gruenflaechen, {
    onEachFeature : function(feature, layer) {
        layer.bindPopup("<p><b>Grünfläche</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Langname</b></th><td>" + feature.properties.LANGNAME + "</td><tr><th><b>Art</b></th><td>" + feature.properties.ART + "</td></tr></tbody></table>");
    }
});
var waldflaechen_mo = L.geoJSON(waldflaechen, {
    onEachFeature : function(feature, layer) {
        layer.bindPopup("<p><b>Waldfäche</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Langname</b></th><td>" + feature.properties.LANGNAME + "</td><tr><th><b>Art</b></th><td>" + feature.properties.ART + "</td></tr></tbody></table>");
    }
});
var kitas_mo = L.geoJSON(kitas, {
    onEachFeature : function(feature, layer) {
        layer.bindPopup("<p><b>Kindertagesstätte</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + feature.properties.NAME + "</td><tr><th><b>Straße</b></th><td>" + feature.properties.STRASSE_HA + "</td></tr><tr><th><b>Postleizahl</b></th><td>" + feature.properties.POSTLEITZA + "</td></tr><tr><th><b>Ort</b></th><td>" + feature.properties.ORT + "</td></tr><tr><th><b>Kreis</b></th><td>" + feature.properties.KREIS + "</td></tr></tbody></table>");
    },
    pointToLayer : function(geoJsonPoint, latlng) {
        return L.marker(latlng).setIcon(kitas_icon);
    }
});
var bezirke_mo = L.geoJSON(bezirke, {
    onEachFeature : function(feature, layer) {
        layer.bindPopup("<p><b>Bezirk</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Bezirksart</b></th><td>" + feature.properties.BEZIRKSAR1 + "</td><tr><th><b>Bezirksname</b></th><td>" + feature.properties.BEZIRKSBEZ + "</td></tr><tr><th><b>Bezirksnummer</b></th><td>" + feature.properties.BEZIRKSNUM + "</td></tr></tbody></table>");
    }
});
var schulen_mo = L.geoJSON(schulen, {
    onEachFeature : function(feature, layer) {
        layer.bindPopup("<p><b>Schule</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + feature.properties.NAME + "</td><tr><th><b>Straße</b></th><td>" + feature.properties.STRASSE_HA + "</td></tr><tr><th><b>Postleizahl</b></th><td>" + feature.properties.POSTLEITZA + "</td></tr><tr><th><b>Ort</b></th><td>" + feature.properties.ORT + "</td></tr><tr><th><b>Ortsteil</b></th><td>" + feature.properties.ORTSTEIL + "</td></tr><tr><th><b>Kreis</b></th><td>" + feature.properties.KREIS + "</td></tr></tbody></table>");
    },
    pointToLayer : function(geoJsonPoint, latlng) {
        return L.marker(latlng).setIcon(school_icon);
    }
});
var spielplaetze_mo = L.geoJSON(spielplaetze, {
    onEachFeature : function(feature, layer) {
        layer.bindPopup("<p><b>Spielplatz</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + feature.properties.LANGNAME + "</td><tr><th><b>Art</b></th><td>" +
            feature.properties.ART + "</td></tr></tbody></table>");
    }
});
var fahrradstaender_mo = L.geoJSON(fahrradstaender, {
    onEachFeature : function(feature, layer) {
        layer.bindPopup("<p><b>Fahrradständer</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Art</b></th><td>" +
            feature.properties.LANGNAME + "</td></tr></tbody></table>");
    },
    pointToLayer : function(geoJsonPoint, latlng) {
        return L.marker(latlng).setIcon(bike_icon);
    }
});
var gur_mo = L.geoJSON(gur, {
    onEachFeature : function(feature, layer) {
        layer.bindPopup("<p><b>Geh- und Radwege</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Art</b></th><td>" + feature.properties.LANGNAME + "</td><tr><th><b>Beschaffenheit</b></th><td>" +
            feature.properties.ART + "</td></tr></tbody></table>");
    }
});
var niederrheinweg_mo = L.geoJSON(niederrheinweg, {
    onEachFeature : function(feature, layer) {
        layer.bindPopup("");
    }
});
var hundekot_mo = L.geoJSON(hundekot, {
    onEachFeature : function(feature, layer) {
        layer.bindPopup("<p><b>Hundekotbeutelspender</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + feature.properties.NAME + "</td><tr><th><b>Kategorie</b></th><td>" + feature.properties.KATEGORIE + "</td></tr><tr><th><b>Straße</b></th><td>" + feature.properties.STRASSE_HA + "</td></tr><tr><th><b>Ort</b></th><td>" + feature.properties.ORT + "</td></tr><tr><th><b>Kreis</b></th><td>" + feature.properties.KREIS + "</td></tr><tr><th><b>Internet</b></th><td>" + "<a target='_blank' href='" + feature.properties.INTERNET + "'>" + feature.properties.INTERNET + "<i class='tiny material-icons'>link</i></a>" + "</td></tr><tr><th><b>Bemerkung</b></th><td>" + feature.properties.BEMERKUNG + "</td></tr></tbody></table>");
    },
    pointToLayer : function(geoJsonPoint, latlng) {
        return L.marker(latlng).setIcon(hundekot_icon);
    }
});
var sitzplaetze_mo = L.geoJSON(sitzplaetze, {
    onEachFeature : function(feature, layer) {
        layer.bindPopup("<p><b>Sitzgelegenheit</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Langname</b></th><td>" + feature.properties.LANGNAME + "</td><tr><th><b>Kategorie</b></th><td>" + feature.properties.KATEGORIE + "</td></tr><tr><th><b>Art</b></th><td>" + feature.properties.ART + "</td></tr></tbody></table>");
    },
    pointToLayer : function(geoJsonPoint, latlng) {
        return L.marker(latlng).setIcon(sitzplaetze_icon);
    }
});
var wasser_mo = L.geoJSON(wasser, {
    onEachFeature : function(feature, layer) {
        layer.bindPopup("<p><b>Wasserfläche</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Langname</b></th><td>" + feature.properties.LANGNAME + "</td><tr><th><b>Art</b></th><td>" +
            feature.properties.ART + "</td></tr></tbody></table>");
    }
});
var kleingaerten_mo = L.geoJSON(kleingaerten, {
    onEachFeature : function(feature, layer) {
        layer.bindPopup("<p><b>Kleingarten</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + feature.properties.LANGNAME + "</td><tr><th><b>Art</b></th><td>" +
            feature.properties.ART + "</td></tr></tbody></table>");
    }
});
var stolpersteine_mo = L.geoJSON(stolpersteine, {
    onEachFeature : function(feature, layer) {
        layer.bindPopup("<p><b>Stolperstein</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" +
            feature.properties.NAME + "</td><tr><th><b>Internet</b></th><td>" +
            "<a target='_blank' href='" + feature.properties.INTERNET + "'>" + feature.properties.INTERNET +
            "<i class='tiny material-icons'>link</i></a>" + "</td></tr><tr><th><b>Bemerkung</b></th><td>" + feature.properties.BEMERKUNG +
            "</td></tr></tbody></table>");
    },
    pointToLayer : function(geoJsonPoint, latlng) {
        return L.marker(latlng).setIcon(stolpersteine_icon);
    }
});

/*
 *Optional einfügbar
 *
 var biotope_mo = L.geoJSON(biotop, {
 onEachFeature: function (feature, layer) {
 layer.bindPopup("<p><b>Biotop</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Art</b></th><td>" + feature.properties.Art + "</td><tr><th><b>Bemerkung</b></th><td>" +
 feature.properties.Bemerkung + "</td></tr></tbody></table>");
 }
 });
 * */

/*
 Style Polygons
 */
gruenflaechen_mo.setStyle({
    fillColor : '#33DC2B',
    color : '#33DC2B',
});
waldflaechen_mo.setStyle({
    fillColor : '#12570F',
    color : '#12570F',
});
spielplaetze_mo.setStyle({
    fillColor : '#B83829',
    color : '#B83829',
});
bezirke_mo.setStyle({
    fillColor : '#b89616',
    color : '#b89616',
});
kleingaerten_mo.setStyle({
    fillColor : '#b8688e',
    color : '#b8688e',
});
niederrheinweg_mo.setStyle({
    fillColor : '#FF45FD',
    color : '#FF45FD',
});

/*
 Marker + Layer für Rathaus und Standesamt

 var marker_rathaus = L.marker([51.453490, 6.626374]).bindPopup('Rathaus Moers');

 var marker_standesamt = L.marker([51.453140, 6.625305], {icon: bike_icon}).bindPopup('Standesamt Moers').addTo(map);


 var einrichtungen = L.layerGroup([marker_rathaus, marker_standesamt]);
 */

/*
 Bäder Datensatz einfügen

 for (i = 0; i < baeder.length; i++) {
 if(baeder[i].SLat !== ""){


 var baeder_4 = L.marker([baeder[4].SLat, baeder[4].SLng]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[4].DocName + "</td></tr><tr><th><b>Kategorie</b></th><td>" + baeder[4].Kategorie1 + "</td></tr><tr><th><b>Internet</b></th><td>" + baeder[4].URL + "</td></tr></tbody></table>");

 }
 }
 */

var baeder_1 = L.marker([ baeder[1].SLat, baeder[1].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[1].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[1].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_4 = L.marker([ baeder[4].SLat, baeder[4].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[4].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[4].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_5 = L.marker([ baeder[5].SLat, baeder[5].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[5].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[5].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_8 = L.marker([ baeder[8].SLat, baeder[8].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[8].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[8].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_9 = L.marker([ baeder[9].SLat, baeder[9].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[9].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[9].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_10 = L.marker([ baeder[10].SLat, baeder[10].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[10].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[10].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_12 = L.marker([ baeder[12].SLat, baeder[12].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[12].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[12].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_13 = L.marker([ baeder[13].SLat, baeder[13].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[13].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[13].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_15 = L.marker([ baeder[15].SLat, baeder[15].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[15].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[15].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_17 = L.marker([ baeder[17].SLat, baeder[17].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[17].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[17].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_18 = L.marker([ baeder[18].SLat, baeder[18].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[18].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[18].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_19 = L.marker([ baeder[19].SLat, baeder[19].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[19].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[19].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_20 = L.marker([ baeder[20].SLat, baeder[20].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[20].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[20].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_21 = L.marker([ baeder[21].SLat, baeder[21].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[21].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[21].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_22 = L.marker([ baeder[22].SLat, baeder[22].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[22].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[22].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_23 = L.marker([ baeder[23].SLat, baeder[23].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[23].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[23].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_25 = L.marker([ baeder[25].SLat, baeder[25].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[25].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[25].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_26 = L.marker([ baeder[26].SLat, baeder[26].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[26].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[26].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_27 = L.marker([ baeder[27].SLat, baeder[27].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[27].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[27].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_28 = L.marker([ baeder[28].SLat, baeder[28].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[28].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[28].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_29 = L.marker([ baeder[29].SLat, baeder[29].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[29].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[29].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_31 = L.marker([ baeder[31].SLat, baeder[31].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[31].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[31].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_32 = L.marker([ baeder[32].SLat, baeder[32].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[32].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[32].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_33 = L.marker([ baeder[33].SLat, baeder[33].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[33].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[33].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_35 = L.marker([ baeder[35].SLat, baeder[35].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[35].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[35].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_36 = L.marker([ baeder[36].SLat, baeder[36].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[36].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[36].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_38 = L.marker([ baeder[38].SLat, baeder[38].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[38].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[38].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_39 = L.marker([ baeder[39].SLat, baeder[39].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[39].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[39].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_40 = L.marker([ baeder[40].SLat, baeder[40].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[40].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[40].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_41 = L.marker([ baeder[41].SLat, baeder[41].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[41].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[41].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_42 = L.marker([ baeder[42].SLat, baeder[42].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[42].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[42].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_43 = L.marker([ baeder[43].SLat, baeder[43].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[43].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[43].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_44 = L.marker([ baeder[44].SLat, baeder[44].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[44].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[44].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_45 = L.marker([ baeder[45].SLat, baeder[45].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[45].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[45].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_46 = L.marker([ baeder[46].SLat, baeder[46].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[46].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[46].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_47 = L.marker([ baeder[47].SLat, baeder[47].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[47].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[47].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_48 = L.marker([ baeder[48].SLat, baeder[48].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[48].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[48].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_49 = L.marker([ baeder[49].SLat, baeder[49].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[49].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[49].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_51 = L.marker([ baeder[51].SLat, baeder[51].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[51].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[51].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_52 = L.marker([ baeder[52].SLat, baeder[52].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[52].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[52].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_54 = L.marker([ baeder[54].SLat, baeder[54].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[54].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[54].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_55 = L.marker([ baeder[55].SLat, baeder[55].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[55].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[55].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_56 = L.marker([ baeder[56].SLat, baeder[56].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[56].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[56].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_57 = L.marker([ baeder[57].SLat, baeder[57].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[57].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[57].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_58 = L.marker([ baeder[58].SLat, baeder[58].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[58].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[58].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_59 = L.marker([ baeder[59].SLat, baeder[59].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[59].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[59].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_60 = L.marker([ baeder[60].SLat, baeder[60].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[60].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[60].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_61 = L.marker([ baeder[61].SLat, baeder[61].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[61].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[61].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_62 = L.marker([ baeder[62].SLat, baeder[62].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[62].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[62].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_63 = L.marker([ baeder[63].SLat, baeder[63].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[63].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[63].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_65 = L.marker([ baeder[65].SLat, baeder[65].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[65].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[65].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_66 = L.marker([ baeder[66].SLat, baeder[66].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[66].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[66].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_67 = L.marker([ baeder[67].SLat, baeder[67].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[67].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[67].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_68 = L.marker([ baeder[68].SLat, baeder[68].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[68].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[68].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_70 = L.marker([ baeder[70].SLat, baeder[70].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[70].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[70].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_71 = L.marker([ baeder[71].SLat, baeder[71].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[71].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[71].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_72 = L.marker([ baeder[72].SLat, baeder[72].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[72].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[72].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_73 = L.marker([ baeder[73].SLat, baeder[73].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[73].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[73].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_74 = L.marker([ baeder[74].SLat, baeder[74].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[74].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[74].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_75 = L.marker([ baeder[75].SLat, baeder[75].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[75].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[75].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_76 = L.marker([ baeder[76].SLat, baeder[76].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[76].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[76].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_77 = L.marker([ baeder[77].SLat, baeder[77].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[77].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[77].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_78 = L.marker([ baeder[78].SLat, baeder[78].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[78].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[78].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_79 = L.marker([ baeder[79].SLat, baeder[79].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[79].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[79].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_80 = L.marker([ baeder[80].SLat, baeder[80].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[80].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[80].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_81 = L.marker([ baeder[81].SLat, baeder[81].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[81].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[81].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_82 = L.marker([ baeder[82].SLat, baeder[82].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[82].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[82].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_83 = L.marker([ baeder[83].SLat, baeder[83].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[83].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[83].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_84 = L.marker([ baeder[84].SLat, baeder[84].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[84].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[84].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_85 = L.marker([ baeder[85].SLat, baeder[85].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[85].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[85].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_86 = L.marker([ baeder[86].SLat, baeder[86].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[86].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[86].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_87 = L.marker([ baeder[87].SLat, baeder[87].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[87].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[87].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_88 = L.marker([ baeder[88].SLat, baeder[88].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[88].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[88].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_89 = L.marker([ baeder[89].SLat, baeder[89].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[89].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[89].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_90 = L.marker([ baeder[90].SLat, baeder[90].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[90].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[90].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_91 = L.marker([ baeder[91].SLat, baeder[91].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[91].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[91].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_92 = L.marker([ baeder[92].SLat, baeder[92].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[92].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[92].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_93 = L.marker([ baeder[93].SLat, baeder[93].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[93].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[93].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_94 = L.marker([ baeder[94].SLat, baeder[94].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[94].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[94].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);
var baeder_95 = L.marker([ baeder[95].SLat, baeder[95].SLng ]).bindPopup("<p><b>Sportstätten und Bäder in Moers</b></p><table class ='bordered responsive-table'><tbody><tr><th><b>Name</b></th><td>" + baeder[95].DocName + "</td><tr><th><b>Kategorie</b></th><td>" + baeder[95].Kategorie1 + "</td></tr></tbody></table>").setIcon(badestaetten_icon);


var baeder_mo = L.layerGroup([
    baeder_1, baeder_4, baeder_5, baeder_8, baeder_9, baeder_10, baeder_12, baeder_13, baeder_15, baeder_17, baeder_18,
    baeder_19, baeder_20, baeder_21, baeder_22, baeder_23, baeder_25, baeder_26, baeder_27, baeder_28, baeder_29, baeder_31, baeder_32, baeder_33,
    baeder_35, baeder_36, baeder_38, baeder_39, baeder_40, baeder_41, baeder_42, baeder_43, baeder_44, baeder_45, baeder_46, baeder_47, baeder_48,
    baeder_49, baeder_51, baeder_52, baeder_54, baeder_55, baeder_56, baeder_57, baeder_58, baeder_59, baeder_60, baeder_61, baeder_62, baeder_63,
    baeder_65, baeder_66, baeder_67, baeder_68, baeder_70, baeder_71, baeder_72, baeder_73, baeder_74, baeder_75, baeder_76, baeder_77,
    baeder_78, baeder_79, baeder_80, baeder_81, baeder_82, baeder_83, baeder_84, baeder_85, baeder_86, baeder_87, baeder_88, baeder_89, baeder_90,
    baeder_91, baeder_92, baeder_93, baeder_94, baeder_95 ]);





/*
 Telpano Datensätze
 */
var telepano_1 = L.marker([ 51.450945, 6.625564 ]).bindPopup('<p><b>Haagstr-Kastell-Klosterstr</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1930&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_2 = L.marker([ 51.451183, 6.625737 ]).bindPopup('<p><b>Klosterstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1204&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_3 = L.marker([ 51.450580, 6.625298 ]).bindPopup('<p><b>Kastell</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1931&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_4 = L.marker([ 51.450159, 6.624992 ]).bindPopup('<p><b>Altes Landratsamt</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1932&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_5 = L.marker([ 51.450066, 6.628353 ]).bindPopup('<p><b>Haagstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano2307&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_6 = L.marker([ 51.450221, 6.629005 ]).bindPopup('<p><b>Uerdingerstr./Haagstr</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1938&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_7 = L.marker([ 51.450707, 6.629218 ]).bindPopup('<p><b>Kiosk West</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1933&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_8 = L.marker([ 51.450877, 6.628442 ]).bindPopup('<p><b>Steinstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1216&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_9 = L.marker([ 51.451018, 6.627881 ]).bindPopup('<p><b>Steinstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1213&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_10 = L.marker([ 51.451115, 6.627412 ]).bindPopup('<p><b>Steinstraße/Burgstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1212&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_11 = L.marker([ 51.450781, 6.627148 ]).bindPopup('<p><b>Burgstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1195&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_12 = L.marker([ 51.451246, 6.627098 ]).bindPopup('<p><b>Steinstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1217&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_13 = L.marker([ 51.451876, 6.626842 ]).bindPopup('<p><b>Pfefferstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1211&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_14 = L.marker([ 51.451457, 6.626454 ]).bindPopup('<p><b>Steinstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1210&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_15 = L.marker([ 51.451938, 6.626457 ]).bindPopup('<p><b>Pfefferstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1201&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_16 = L.marker([ 51.451986, 6.626227 ]).bindPopup('<p><b>Kirchstraße/Pfefferstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1202&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_17 = L.marker([ 51.452285, 6.626351 ]).bindPopup('<p><b>Kirchstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1200&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_18 = L.marker([ 51.452530, 6.626689 ]).bindPopup('<p><b>Kirchstraße/Friedrichstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1203&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_19 = L.marker([ 51.452833, 6.626791 ]).bindPopup('<p><b>Kirchstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1220&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_20 = L.marker([ 51.453022, 6.627016 ]).bindPopup('<p><b>Wallzentrum/Kirchstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1939&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_21 = L.marker([ 51.453171, 6.627628 ]).bindPopup('<p><b>Unterwallstraße/Neuer Wall</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1197&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_22 = L.marker([ 51.452433, 6.626988 ]).bindPopup('<p><b>Friedrichstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1196&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_23 = L.marker([ 51.452091, 6.627387 ]).bindPopup('<p><b>Friedrichstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1214&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_24 = L.marker([ 51.451576, 6.625789 ]).bindPopup('<p><b>Steinstraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1206&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_25 = L.marker([ 51.452368, 6.625682 ]).bindPopup('<p><b>Neumarkt</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1198&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_26 = L.marker([ 51.451775, 6.627988 ]).bindPopup('<p><b>Hans-Dieter-Hüsch-Platz/Wallzentrum</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1207&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_27 = L.marker([ 51.451798, 6.624649 ]).bindPopup('<p><b>Neustraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1208&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_28 = L.marker([ 51.451917, 6.624054 ]).bindPopup('<p><b>Neustraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1209&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_29 = L.marker([ 51.452099, 6.623268 ]).bindPopup('<p><b>Neustraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1934&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_30 = L.marker([ 51.452184, 6.622826 ]).bindPopup('<p><b>Neustraße</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1935&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_31 = L.marker([ 51.452336, 6.622233 ]).bindPopup('<p><b>Zum Alten Brauhaus</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1194&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_32 = L.marker([ 51.451530, 6.626131 ]).bindPopup('<p><b>Altmarkt</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1936&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_33 = L.marker([ 51.453268, 6.626469 ]).bindPopup('<p><b>Rathausvorplatz</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1937&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_34 = L.marker([ 51.449827, 6.624706 ]).bindPopup('<p><b>Moerser Schloss</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1205&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_35 = L.marker([ 51.451698, 6.625235 ]).bindPopup('<p><b>Neumarkt</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1199&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_36 = L.marker([ 51.451752, 6.627585 ]).bindPopup('<p><b>Hans-Dieter-Hüsch-Platz</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano1199&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_37 = L.marker([ 51.449449, 6.624501 ]).bindPopup('<p><b>Schlossausstellung</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano3169&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_38 = L.marker([ 51.453113, 6.631724 ]).bindPopup('<p><b>Hanns-Dieter-Hüsch-Bildungszentrum</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano3171&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_39 = L.marker([ 51.452966, 6.63131 ]).bindPopup('<p><b>VHS Vorplatz</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano3172&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_40 = L.marker([ 51.450403, 6.640946 ]).bindPopup('<p><b>Bahnhofsvorplatz</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano3165&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);
var telepano_41 = L.marker([ 51.453679, 6.62541 ]).bindPopup('<p><b>Rathauspark</b><br><a target="_blank" href="https://moers-stadtrundgang.telepano.de/tour.html?s=pano3166&skipintro&html5=only">Zur 360°-Ansicht</a></p>').setIcon(telepano_360_grad_icon);

var telepano_360_grad = L.layerGroup([ telepano_1, telepano_2, telepano_3, telepano_4, telepano_5, telepano_6, telepano_7, telepano_8, telepano_9, telepano_10, telepano_11, telepano_12, telepano_13, telepano_14, telepano_15, telepano_16, telepano_17, telepano_18, telepano_19, telepano_20, telepano_21, telepano_22, telepano_23, telepano_24, telepano_25, telepano_26, telepano_27, telepano_28, telepano_29, telepano_30, telepano_31, telepano_32, telepano_33, telepano_34, telepano_35, telepano_36, telepano_37, telepano_38, telepano_39, telepano_40, telepano_41 ]);

/*
 Layer overlayMaps mit den Polygons und Markern fuellen
 */
var overlayMaps = {
    "Adolfinum Gymnasium" : adolfinum_layer,
    "360°-Stadtrundgang" : telepano_360_grad,
    "Bezirke" : bezirke_mo,
    "Parkflächen" : gruenflaechen_mo,
    "Waldflächen" : waldflaechen_mo,
    "Wasserflächen" : wasser_mo,
    "Kleingärten" : kleingaerten_mo,
    "Kindertagesstätten" : kitas_mo,
    "Schulen" : schulen_mo,
    "Bäder und Sportstätten" : baeder_mo,
    "Spielplätze" : spielplaetze_mo,
    "Fahrradständer" : fahrradstaender_mo,
    "Geh- und Radwege" : gur_mo,
    "Hundekotbeutelspender" : hundekot_mo,
    "Sitzgelegenheiten" : sitzplaetze_mo,
    "Stolpersteine" : stolpersteine_mo,
    "Niederrheinweg":niederrheinweg_mo,
//    "Biotope": biotope_mo,
};

/*
 Control
 */
var thunderforest = L.tileLayer('https://{s}.tile.thunderforest.com/cycle/{z}/{x}/{y}.png', {
    attribution : '&copy; <a href="http://www.thunderforest.com">Thunderforest</a> contributors'
});

var baseMaps = {
    "OpenStreetMap" : openstreetmap,
    "Fahrradkarte" : thunderforest
};

/*
 Die Layerverwaltung standardmäßig ausklappen
 */
L.control.layers(baseMaps, overlayMaps, {
    "collapsed" : false
}).addTo(map_full);

/*
 Maßstab anzeigen
 */
L.control.scale().addTo(map_full);

/*
 The MIT License (MIT)

 Copyright (c) 2014 coryasilva

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */